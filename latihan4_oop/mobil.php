<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOP PHP</title>
</head>
<body>
<?php
   class Mobil 
      {
      public $roda = 4;
      public function jalan()
      {
         echo "Mobil berjalan";
         }
    }
$mini = new Mobil();
$mini->jalan(); // menampilkan echo 'Mobil berjalan'
echo "<br>";
echo $mini->roda; // 4
?>
</body>
</html>