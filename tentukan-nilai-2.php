<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
      /**
       * Jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka
       * akan mereturn String “Sangat Baik” Selain itu jika parameter integer lebih besar sama dengan 70 
       * dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number 
       * lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” 
       * selain itu maka akan mereturn string “Kurang”
       */


        function tentukan_nilai($number)
        {
         $output="";
         if ($number>= 85 && $number <=100)
         { $output = "Sangat Baik <br>";
         } else if ($number>= 70 && $number <85) {
            $output = "Baik <br>";
         } else if ($number>= 60 && $number <70) {
            $output = "Cukup <br>";
         } else {
            $output = "Kurang";
         }
         //  kode disini
         return $output;
        }
        
        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang  
        
    ?>

</body>
</html>