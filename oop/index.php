<?php

//require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new Animal("shaun");

echo "Nama Kambing : $sheep->name <br>";// "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 2
echo "Jenis Darah : $sheep->cold_blooded <br> <br>"; // false

$sungokong = new Ape("kera sakti");

echo "Nama Lain Sungokong : $sungokong->name <br>"; 
echo "Jumlah Kaki : $sungokong->legs <br>";
$sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");

echo "<br> Nama Lain Kodok : $kodok->name <br>";
echo "Jumlah Kaki : $kodok->legs <br>";
$kodok->jump(); // "hop hop"

?>